// const { fighter } = require('../models/fighter');
const { validateFighterInput } = require('../validation/fighter.validation');
const { getObjectValuesAsString } = require('../services/objectValues');

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { errors, isValid } = validateFighterInput(req.body);

  if (!isValid) {
    return res.status(400).json({
      error: true,
      message: getObjectValuesAsString(errors),
    });
  }

  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  const { errors, isValid } = validateFighterInput(req.body);

  if (!isValid) {
    return res.status(400).json({
      error: true,
      message: getObjectValuesAsString(errors),
    });
  }

  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
